# Deciphering the impact of azithromycin on microbiota of patients included in the ALLOZITHRO study.

# Purpose
Relapse after allogeneic hematopoietic stem cell transplantation remains the first cause of death and understanding its mechanisms is an unmet clinical need. Azithromycin evaluated to prevent pulmonary graft-versus-host disease, was associated with relapse in a previous phase 3 randomized placebo-controlled trial. 

The aim of this repository is to store source codes and data used to analyse feces samples from patients included in the [ALLOZITHRO-study](https://clinicaltrials.gov/ct2/show/NCT01959100).

To be sure to reproduce easily the analyses make sure to clone in repo in `~/Git/` directory and to create the `~/tmp` directory for the ouputs. Some path may need to be changed within each scripts to make sure the inputs and outputs are found.

# Repository
## data

Contains processed data (metabolome, virome and bacteriome) used for the analyses. Also contains clinical, metabolites and taxonomy metadata and intermediate results stored in `.csv` format for convenience. Intermediate results can be reproduced by the corresponding scripts in the scripts section.

## resHTML
Stores `HTML` outputs for Gitlab pages.

- [Supplemental HTML file 1: integrative network of feces variables](https://nivall.gitlab.io/azimutfeces/supNetwork.html)
- [Supplemental HTML file 2: integrative network of blood and feces variables](https://nivall.gitlab.io/azimutfeces/networkBlood.html)

## results
Stores `CSV` files that report lists of results discussed in the paper

- `SupplementalCSV1.csv` : Raw and corrected P-values Kruskall-Wallis test comparing invertebrates hosts viruses between gut microbiota enterotypes
- `SupplementalCSV2.csv` : Raw and corrected P-values Kruskall-Wallis test comparing bacteriophages hosts viruses between gut microbiota enterotypes
- `SupplementalCSV3.csv` : Raw and corrected P-values Kruskall-Wallis test comparing plants hosts viruses between gut microbiota enterotypes
- `SupplementalCSV4.csv` : Raw and corrected P-values Kruskall-Wallis test comparing eukaryotes hosts viruses between gut microbiota enterotypes
- `SupplementalCSV5.csv` : Correlations between phylotypes frequencies, 99 enterotype-associated metabolites levels and viral species frequencies

## guixconfig (see Computing environment below)
Contains files to load `GUIX` session in linux environment.

## scripts
Contains all scripts used for downstream analyses.

# Computing environment
To reproduce analyses, scripts can be ran in a [GNU Guix environment](https://guix.gnu.org/en/about/) with the `channels.scm` and `manifest.scm` files. Use the command below to run `R`.

```
guix time-machine -C channels.scm -- environment -m manifest.scm -- R
```

Additionnal informations on Guix environment can be found in the following ressources:

- [Our git repository](https://gitlab.com/nivall/guixreprodsci)
- [Our paper: doi https://doi.org/10.1038/s41597-022-01720-9](https://doi.org/10.1038/s41597-022-01720-9)
- [Guix HPC website](https://hpc.guix.info/)
