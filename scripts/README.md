»# Scripts folder
This folder stores scripts used for data management and data analyses.

## Bacterial (`bacterial`)
- Pipeline to import PCoA clusters (`bact_getpcoa.R`)
- PCoA analyses and characteristics of enterotypes (`bact_pcoa.R`)

## Plots theme configurations (`config_plots.R`)

## Data import (`data_import`)
- Import viral data as abundances among host's reads (`import_allviral.R`)
- Import and datamanagement of clinical metadata (`import_clinical_md.R`)
- Import mass cytometry data from [Blood paper](
https://doi.org/10.1182/blood.2022016926
). Raw data are fetched from [Software Heritage Repository](https://archive.softwareheritage.org/swh:1:dir:16a829f98f9d8d707343b3b48bc13f5d641998d9) and processed (`import_cytof.R`)
- Import feces metabolomic data (remove NA > 50% samples and imput for remaining NA) (`import_metabo.R`)
- Import plasma metabolomic data from [Blood paper](
https://doi.org/10.1182/blood.2022016926
). Raw data are fetched from [Software Heritage Repository](https://archive.softwareheritage.org/swh:1:dir:16a829f98f9d8d707343b3b48bc13f5d641998d9) and processed (`import_metaboBlood.R`)
- Import OTU frequencies using `phyloseq` package (`import_physeq_bact.R`)
- Import viral species frequencies using `phyloseq` package (`import_physeq_vir.R`)
- Import variable annotations and identifications (`import_variableAnnot.R`)

## Custom functions (`functions.R`)

## Feces multiomics analyses (`multiom_analyses`)
- Enterotype and OTU composing enterotypes correlations with viral species and metabolites (`bact_pcoa_corr.R`)
- OTU associated with relapse and integratition with viral species and metabolites (`bact_pcoa_grouprel12.R`)
- Compute correlation matrix of all variables and keep significant results. The output is stored in `azimutfeces/data/cormat.csv` (`corMatCompute.R`)
- Compare feces multiomics between enterotype 2 and enterotype 3 (`multiomic_entero2ventero3.R`)
- Compare feces multiomics between enterotype 2 and agregated other enterotypes (`multiomic_entero2vother.R`)
- Evaluate other omics to compare groups (AZM/PLA) and outcome (Relapse/CR) (`multiomic_relazm_contrib.R`)
- Describe samples available according to clinical metadata (`sample_available_feces.R`)

## Blood and feces multiomics analyses 
- Enterotypes and OTU associated with relapse analyses with T cell function and plasma metabolites data (`multiomBlood.R`)

## Preprocess scripts from `FASTQ` to used data
Stores scripts used for 16S sequencing and metagenomic pipelines

## Viral analyses (`viral`)
- Analyses of viral species PCoA clusters with all reads (`vir_all_pcoa.R`)
- Pipeline to compute PCoA clusters (`vir_getPCoA.R`)
- Analysis of viral species PCoA clusters with bacteriophages reads (`vir_phages_pcoa.R`)
- Merge DNA and RNA extracted data. The output is stored in `azimutfeces/data/viral/otu_virmerged.csv` (`virus_dnarna_merge.R`)
