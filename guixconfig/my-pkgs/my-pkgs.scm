(define-module (my-pkgs)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages bioconductor)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages commencement)
  #:use-module ((gnu packages compression) #:prefix compression:)
  #:use-module (gnu packages geo)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix build-system r)
  #:use-module (guix download)
  #:use-module (guix licenses)
  )

;; khroma
(define-public r-khroma
  (let ((commit "c7eaf45d5127a3807e3bc7a648dd463d27030361") (revision "1"))
    (package
     (name "r-khroma")
     (version (git-version "1.9.0.9000" revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/tesselle/khroma")
	     (commit commit)))
       (file-name (git-file-name name version))
       (sha256
	(base32 "0f5cg443amizpra6jn0fnhzwfd3hkc97bcf9s159bagqav780z7v"))))
     (properties `((upstream-name . "khroma")))
     (build-system r-build-system)
     (native-inputs (list r-knitr))
     (home-page "https://github.com/tesselle/khroma")
     (synopsis "Colour Schemes for Scientific Data Visualization")
     (description
      "Colour schemes ready for each type of data (qualitative,
diverging or sequential), with colours that are distinct for all
people, including colour-blind readers.  This package provides an
implementation of Paul Tol (2018) and Fabio Crameri (2018)
<doi:10.5194/gmd-11-2541-2018> colour schemes for use with 'graphics'
or 'ggplot2'.  It provides tools to simulate colour-blindness and to
test how well the colours of any palette are identifiable.  Several
scientific thematic schemes (geologic timescale, land cover, FAO
soils, etc.) are also implemented.")
     (license license:gpl3+))))

;; needed for ggalt
(define-public r-proj4
  (let ((commit "0877ab8b38f9db1a294bca16a5c27f3b22b768e5") (revision "1"))
    (package
     (name "r-proj4")
     (version (git-version "1.0-11" revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/s-u/proj4")
	     (commit commit)))
       (file-name (git-file-name name version))
       (sha256
	(base32 "0fgdbp91nm9q524ci31wfd0m87aj9vzajzrma4xhbbyrvjjr1pgv"))))
     (properties `((upstream-name . "proj4")))
     (build-system r-build-system)
     (inputs (list proj.4 compression:zlib))
     (native-inputs (list pkg-config))
     (home-page "https://github.com/s-u/proj4")
     (synopsis
      "A simple interface to the PROJ.4 cartographic projections library")
     (description
      "This package provides a simple interface to lat/long projection and datum
transformation of the PROJ.4 cartographic projections library.  It allows
transformation of geographic coordinates from one projection and/or datum to
another.")
     (license license:gpl2))))

;; ggalt
(define-public r-ggalt
  (let ((commit "e46e54bbf800ab6aff88f32097d1a42fd43c097e") (revision "1"))
    (package
     (name "r-ggalt")
     (version (git-version "0.6.2" revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/hrbrmstr/ggalt")
	     (commit commit)))
       (file-name (git-file-name name version))
       (sha256
	(base32 "1rzhpqsjf04290skvfnbpvsc0w9iqvy2pas7dwsdp6im5xnjvgdr"))))
     (properties `((upstream-name . "ggalt")))
     (build-system r-build-system)
     (propagated-inputs
      (list r-ash
	    r-dplyr
	    r-extrafont
	    r-ggplot2
	    r-gtable
	    r-kernsmooth
	    r-maps
	    r-mass
	    r-plotly
	    r-plyr
	    r-proj4
	    r-rcolorbrewer
	    r-scales
	    r-tibble))
     (native-inputs (list r-knitr))
     (home-page "https://github.com/hrbrmstr/ggalt")
     (synopsis
      "Extra Coordinate Systems, 'Geoms', Statistical Transformations,
Scales and Fonts for 'ggplot2'")
     (description
      "This package provides a compendium of new geometries, coordinate
systems, statistical transformations, scales and fonts for 'ggplot2',
including splines, 1d and 2d densities, univariate average shifted
histograms, a new map coordinate system based on the 'PROJ.4'-library
along with geom_cartogram() that mimics the original functionality of
geom_map(), formatters for \"bytes\", a stat_stepribbon() function,
increased 'plotly' compatibility and the 'StateFace' open source font
'ProPublica'.  Further new functionality includes lollipop charts,
dumbbell charts, the ability to encircle points and
coordinate-system-based text annotations.")
     (license license:expat))))

;; needed for gtsummary
(define-public r-broom-helpers
(let ((commit "6a5f6be7110b30ed12e2cf23dc0de399c0dd6ef0") (revision "1"))
  (package
    (name "r-broom-helpers")
    (version (git-version "1.9.0.9000" revision commit))
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/larmarange/broom.helpers")
               (commit commit)))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0z796qxmarc6f4c6jw9924gklxsabaxv33dlblvd270bnwx6cyyj"))))
    (properties `((upstream-name . "broom.helpers")))
    (build-system r-build-system)
    (propagated-inputs
      (list r-broom
            r-cli
            r-dplyr
            r-labelled
            r-lifecycle
            r-purrr
            r-rlang
            r-stringr
            r-tibble
            r-tidyr))
    (native-inputs (list r-knitr))
    (home-page "https://github.com/larmarange/broom.helpers")
    (synopsis "Helpers for Model Coefficients Tibbles")
    (description
      "This package provides suite of functions to work with
regression model 'broom::tidy()' tibbles.  The suite includes
functions to group regression model terms by variable, insert
reference and header rows for categorical variables, add variable
labels, and more.")
    (license license:gpl3))))

(define-public r-bigd
  (package
   (name "r-bigd")
   (version "0.2.0")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "bigD" version))
     (sha256
      (base32 "0pzzqqazn1nv2b613vzdyhxvr454lyqq8235jydia6r3k86fpadw"))))
   (properties `((upstream-name . "bigD")))
   (build-system r-build-system)
   (home-page "https://github.com/rich-iannone/bigD")
   (synopsis "Flexibly Format Dates and Times to a Given Locale")
   (description
    "Format dates and times flexibly and to whichever locales make
sense.  Parses dates, times, and date-times in various formats
(including string-based ISO 8601 constructions).  The formatting
syntax gives the user many options for formatting the date and time
output in a precise manner.  Time zones in the input can be expressed
in multiple ways and there are many options for formatting time zones
in the output as well.  Several of the provided helper functions allow
for automatic generation of locale-aware formatting patterns based on
date/time skeleton formats and standardized date/time formats with
varying specificity.")
   (license license:expat)))


;; gtsummary
(define-public r-gtsummary
  (let ((commit "3c63f7200630e22d1496e9c4b0e8bb50e2fa5807") (revision "1"))
    (package
     (name "r-gtsummary")
     (version (git-version "1.6.2.9000" revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/ddsjoberg/gtsummary")
	     (commit commit)))
       (file-name (git-file-name name version))
       (sha256
	(base32 "086zaiz7q1r79gzjaaapn8kfllx70cxv52z1sy2ckja21nqv72ck"))))
     (properties `((upstream-name . "gtsummary")))
     (build-system r-build-system)
     (propagated-inputs
      (list r-broom
            r-broom-helpers
            r-cli
            r-dplyr
            r-forcats
            r-glue
            r-gt
            r-knitr
            r-lifecycle
            r-purrr
            r-rlang
            r-stringr
            r-tibble
            r-tidyr))
     (native-inputs (list r-knitr))
     (home-page "https://github.com/ddsjoberg/gtsummary")
     (synopsis "Presentation-Ready Data Summary and Analytic Result Tables")
     (description
      "Creates presentation-ready tables summarizing data sets,
regression models, and more.  The code to create the tables is concise
and highly customizable.  Data frames can be summarized with any
function, e.g.  mean(), median(), even user-written functions.
Regression models are summarized and include the reference rows for
categorical variables.  Common regression models, such as logistic
regression and Cox proportional hazards regression, are automatically
identified and the tables are pre-filled with appropriate column
headers.")
     (license license:expat))))


;; gt
(define-public r-gt
  (let ((commit "d8c883bd387530e9fdde0a8fd726fa8feacf3bb0") (revision "1"))
    (package
     (name "r-gt")
     (version (git-version "0.7.0.9000" revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/rstudio/gt")
	     (commit commit)))
       (file-name (git-file-name name version))
       (sha256
	(base32 "1a447b0ip10wxy8m8dmahx13vjlwwarn0k1cq1532r8039g20adk"))))
     (properties `((upstream-name . "gt")))
     (build-system r-build-system)
     (propagated-inputs
      (list r-base64enc
            r-bigd
            r-bitops
            r-cli
            r-commonmark
            r-dplyr
            r-fs
            r-ggplot2
            r-glue
            r-htmltools
            r-magrittr
            r-rlang
            r-sass
            r-scales
            r-tibble
            r-tidyselect))
     (home-page "https://github.com/rstudio/gt")
     (synopsis "Easily Create Presentation-Ready Display Tables")
     (description
      "Build display tables from tabular data with an easy-to-use set
of functions.  With its progressive approach, we can construct display
tables with a cohesive set of table parts.  Table values can be
formatted using any of the included formatting functions.  Footnotes
and cell styles can be precisely added through a location targeting
system.  The way in which 'gt' handles things for you means that you
don't often have to
worry about the fine details.")
     (license license:expat))))

;; visnetwork
(define-public r-visnetwork
  (let ((commit "5dc43a8b45a6fbe1089a0f19d5f0ec4a5ab16ccb") (revision "1"))
    (package
     (name "r-visnetwork")
     (version (git-version "2.1.2" revision commit))
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
	     (url "https://github.com/datastorm-open/visNetwork")
	     (commit commit)))
       (file-name (git-file-name name version))
       (sha256
	(base32 "0nqdbzdzwki5bjnbh1sizprmbkxnxnp2d9sj8d7g6c6q9qqbc5sg"))))
     (properties `((upstream-name . "visNetwork")))
     (build-system r-build-system)
     (propagated-inputs (list r-htmltools r-htmlwidgets r-jsonlite r-magrittr))
     (native-inputs (list r-knitr))
     (home-page "https://github.com/datastorm-open/visNetwork")
     (synopsis "Network Visualization using 'vis.js' Library")
     (description
      "This package provides an R interface to the 'vis.js' JavaScript
charting library.  It allows an interactive visualization of
networks.")
     (license license:expat))))

;; needed for riskregression
(define-public r-mets
  (package
   (name "r-mets")
   (version "1.3.1")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "mets" version))
            (sha256
             (base32
              "0kzmbwhziknk4djql88kvanb0pncnxq0c6b7rsfd1z7c0c8kpyv0"))))
   (properties `((upstream-name . "mets")))
   (build-system r-build-system)
   (arguments
    (list
     #:phases
     `(modify-phases %standard-phases
         (add-after 'unpack 'remove-inst-doc-binreg
           (lambda _
             (delete-file "inst/doc/binreg.Rmd"))))))
   (propagated-inputs (list r-lava
			    r-mvtnorm
			    r-numderiv
			    r-rcpp
			    r-rcpparmadillo
			    r-survival
			    r-timereg))
   (native-inputs (list gfortran r-knitr r-rmarkdown))
   (home-page "https://kkholst.github.io/mets/")
   (synopsis "Analysis of Multivariate Event Times")
   (description
    "Implementation of various statistical models for multivariate event history data
<doi:10.1007/s10985-013-9244-x>.  Including multivariate cumulative incidence
models <doi:10.1002/sim.6016>, and bivariate random effects probit models
(Liability models) <doi:10.1016/j.csda.2015.01.014>.  Also contains two-stage
binomial modelling that can do pairwise odds-ratio dependence modelling based
marginal logistic regression models.  This is an alternative to the alternating
logistic regression approach (ALR).")
   (license license:gpl2+)))

(define-public r-publish
  (package
   (name "r-publish")
   (version "2020.12.23")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "Publish" version))
            (sha256
             (base32
              "1qpv5hj9agmc4hrpskqk0lns8bh8w3j27d4ckh5y7gh1532qzad7"))))
   (properties `((upstream-name . "Publish")))
   (build-system r-build-system)
   (propagated-inputs (list r-data-table r-lava r-multcomp r-prodlim r-survival))
   (home-page "https://cran.r-project.org/package=Publish")
   (synopsis
    "Format Output of Various Routines in a Suitable Way for Reports and Publication")
   (description
    "This package provides a bunch of convenience functions that
transform the results of some basic statistical analyses into table
format nearly ready for publication.  This includes descriptive
tables, tables of logistic regression and Cox regression results as
well as forest plots.")
   (license license:gpl2+)))

(define-public r-timereg
  (package
   (name "r-timereg")
   (version "2.0.2")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "timereg" version))
            (sha256
             (base32
              "0n9mvdki50j0nbmbs1r91slf7j9nz4cwglscn71kckadsgjjpxmi"))))
   (properties `((upstream-name . "timereg")))
   (build-system r-build-system)
   (propagated-inputs (list r-lava r-numderiv r-survival))
   (home-page "https://github.com/scheike/timereg")
   (synopsis "Flexible Regression Models for Survival Data")
   (description
    "Programs for Martinussen and Scheike (2006), `Dynamic Regression Models for
Survival Data', Springer Verlag.  Plus more recent developments.  Additive
survival model, semiparametric proportional odds model, fast cumulative
residuals, excess risk models and more.  Flexible competing risks regression
including GOF-tests.  Two-stage frailty modelling.  PLS for the additive risk
model.  Lasso in the ahaz package.")
   (license license:gpl2+)))

;; riskregression
(define-public r-riskregression
  (let ((commit "70a20fa90063629dbf54ffc36b24bf6065d14305")
	(revision "1"))
    (package
     (name "r-riskregression")
     (version (git-version "2022.09.29" revision commit))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/tagteam/riskRegression")
                    (commit commit)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "18d6s59l2fdzb6m31hzw03kpnmrn9121il4dvmhp6acndcps3pm3"))))
     (properties `((upstream-name . "riskRegression")))
     (build-system r-build-system)
     (propagated-inputs (list r-cmprsk
			      r-data-table
			      r-doparallel
			      r-foreach
			      r-ggplot2
			      r-lattice
			      r-lava
			      r-mets
			      r-mvtnorm
			      r-plotrix
			      r-prodlim
			      r-publish
			      r-ranger
			      r-rcpp
			      r-rcpparmadillo
			      r-rms
			      r-survival
			      r-timereg))
     (home-page "https://github.com/tagteam/riskRegression")
     (synopsis
      "Risk Regression Models and Prediction Scores for Survival Analysis with Competing Risks")
     (description
      "Implementation of the following methods for event history analysis.  Risk
regression models for survival endpoints also in the presence of competing risks
are fitted using binomial regression based on a time sequence of binary event
status variables.  A formula interface for the Fine-Gray regression model and an
interface for the combination of cause-specific Cox regression models.  A
toolbox for assessing and comparing performance of risk predictions (risk
markers and risk prediction models).  Prediction performance is measured by the
Brier score and the area under the ROC curve for binary possibly time-dependent
outcome.  Inverse probability of censoring weighting and pseudo values are used
to deal with right censored data.  Lists of risk markers and lists of risk
models are assessed simultaneously.  Cross-validation repeatedly splits the
data, trains the risk prediction models on one part of each split and then
summarizes and compares the performance across splits.")
     (license license:gpl2+))))

;; coda4microbiome
(define-public r-coda4microbiome
  (package
   (name "r-coda4microbiome")
   (version "0.1.4")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "coda4microbiome" version))
            (sha256
             (base32
              "1z8bdhnslw13rmdw1jnj5ldr6jmn8grmzn5mhsy96r8x9xyzk5b5"))))
   (properties `((upstream-name . "coda4microbiome")))
   (build-system r-build-system)
   (propagated-inputs (list r-corrplot
                            r-ggplot2
                            r-ggpubr
                            r-glmnet
                            r-plyr
                            r-proc))
   (native-inputs (list r-knitr))
   (home-page "https://malucalle.github.io/coda4microbiome/")
   (synopsis "Compositional Data Analysis for Microbiome Studies")
   (description
    "This package provides functions for microbiome data analysis that take into
account its compositional nature.  Performs variable selection through penalized
regression for both, cross-sectional and longitudinal studies, and for binary
and continuous outcomes.")
   (license expat)))
