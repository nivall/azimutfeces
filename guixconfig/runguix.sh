#!/bin/sh
#  runguix.sh
#  Run this script to build and use the environment defined by manifest.scm in the state defined in channels.scm   

guix time-machine -C channels.scm -- shell -m manifest.scm -L my-pkgs
