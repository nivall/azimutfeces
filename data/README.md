# Data folder

## Bacterial data (`bacterial`)
Stores processed data from 16S sequencing
- OTU abundances (`otu_bact.csv`) 
- Alpha diversity (`bact_alphadiv.csv`)

## Correlation matrix (`cormat.csv`)
Processed correlation matrix stored to avoid time consuming computing

## Feces metabolomic (`metabolomic`)
Stores feces metabolomic data

## Metadata (`metadata`)
Stores metabolomic data 
- Antibiotic intake during the procedure (`azimut_atb.csv`)
- Clinical metadata (`azimut_md_feces.csv`)
- Mass cytometry identification of T cells functionnal clusters (`cytofFun.csv`)
- Mass cytometry identification of PBMC subsets (`cytofPheno.csv`)
- Metabolites identification and pathway (`metabo_path.csv`)

## Taxonomic identification (`tax`)
Stores taxonomic identification tables
- Bacterial taxonomy (`tax_bact.csv`)
- Viral taxonomy (`tax_vir.csv`)

## Viral data (`viral`)
Stores data tables of viruses reads (in read per million) from metagenomic assay (even if files are named `otu`)
- Merged DNA and RNA extracted data (`otu_virmerged.csv`)
- Data from DNA extraction (`otu_virodna.csv`)
- Data from RNA extraction (`otu_virorna.csv`)
- Alpha diversity (`viral_alphadiv.csv`)
